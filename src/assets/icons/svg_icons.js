export const app_logo = color => {
    return `
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="240.949px" height="210.125px" viewBox="0 0 240.949 210.125" enable-background="new 0 0 240.949 210.125"
	 xml:space="preserve">
<g>
	<path fill=${color} d="M120.512,0L120.512,0c-0.013,0-0.025,0-0.038,0c-0.014,0-0.025,0-0.039,0l0,0
		C-4.969,0.031,0.027,63.258,0.027,98.25c0,46.25,14.375,86.5,63.875,86.5c28.046,0,36.249-4.806,44.157-4.806
		c35.092,0,29.967,30.181,48.092,30.181s7.125-21.875,51.258-32.121c24.867-6.379,33.512-44.837,33.512-79.754
		C240.922,63.258,245.918,0.031,120.512,0z M215.936,94.242c-0.008,1.371-0.014,2.708-0.014,4.008
		c0,16.391-2.123,31.371-5.979,42.182c-2.961,8.301-3.842,9.244-8.629,13.322c-4.787,4.079-30.453,8.079-44.787,5.079
		c-14.332-3-37.5-8.333-62.709-2.031c-6.313,1.313-14.17,2.948-29.915,2.948c-18.224,0-25.192-7.504-29.228-14.039
		c-6.313-10.225-9.648-26.636-9.648-47.461c0-1.3-0.006-2.637-0.013-4.008c-0.104-20.848,0.72-36.507,12.335-48.184
		C51.015,32.32,79.66,25.04,120.185,25h0.018h0.271h0.031c40.666,0.01,69.398,7.293,83.094,21.059
		C215.215,57.735,216.039,73.395,215.936,94.242z"/>
	<circle fill=${color} cx="120.695" cy="94.833" r="15"/>
	<circle fill=${color} cx="174.695" cy="94.833" r="15"/>
	<circle fill=${color} cx="66.695" cy="94.833" r="15"/>
</g>
</svg>`;
};
export function chat_icon_line(color) {
    return `<?xml version="1.0" encoding="utf-8"?>
	<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="49px" height="54.75px" viewBox="0 0 49 54.75" enable-background="new 0 0 49 54.75" xml:space="preserve">
	<g>
		<g>
			<path fill=${color} d="M24.5,0C10.969,0,0,10.969,0,24.5c0,6.577,2.592,12.549,6.809,16.949L0.791,54.75l14.023-7.739
				C17.785,48.291,21.061,49,24.5,49C38.031,49,49,38.031,49,24.5S38.031,0,24.5,0z M24.5,43C24.5,43,6,43,6,24.5
				C6,14.299,14.299,6,24.5,6S43,14.299,43,24.5S34.701,43,24.5,43z"/>
			<circle fill=${color} cx="13.5" cy="24.5" r="3"/>
			<circle fill=${color} cx="24.5" cy="24.5" r="3"/>
			<circle fill=${color} cx="35.5" cy="24.5" r="3"/>
		</g>
	</g>
	</svg>`;
}
export function login_user(color) {
    return `<?xml version="1.0" encoding="utf-8"?>
	<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
	<rect display="none" fill="#9B7E90" width="100" height="100"/>
	<g>
		<g>
			<path fill=${color} d="M47,50.488h6c5.522,0,10-4.478,10-10v-6c0-5.522-4.478-10-10-10h-6c-5.522,0-10,4.478-10,10v6
				C37,46.011,41.477,50.488,47,50.488z M43,34.488c0-2.206,1.794-4,4-4h6c2.206,0,4,1.794,4,4v6c0,2.206-1.794,4-4,4h-6
				c-2.206,0-4-1.794-4-4V34.488z"/>
			<path fill=${color} d="M74.985,70.471c-0.01-8.829-7.168-15.982-15.999-15.982H41.014c-8.831,0-15.989,7.153-15.999,15.982
				l-0.029,0.018v1.023c0,2.21,1.791,4,4,4h11c1.104,0,2-0.896,2-2v-2c0-1.104-0.896-2-2-2h-8.923
				c0.493-5.057,4.767-9.023,9.951-9.023h17.972c5.185,0,9.458,3.967,9.95,9.023h-15.95c-1.105,0-2,0.896-2,2v2c0,1.104,0.895,2,2,2
				h18.028c2.209,0,4-1.79,4-4v-1.023L74.985,70.471z"/>
		</g>
	</g>
	<path display="none" fill="#7E879D" d="M70.496,33.002H60.5c-6.627,0-12,5.373-12,12V47H19.504c-1.104,0-2,0.895-2,2v2
		c0,1.104,0.896,2,2,2h7v4c0,1.104,0.896,2,2,2h6c1.104,0,2-0.896,2-2v-4H48.5v1.998c0,6.627,5.373,12,12,12h9.996
		c6.627,0,12-5.373,12-12v-9.996C82.496,38.375,77.123,33.002,70.496,33.002z M76.496,54.998c0,3.309-2.691,6-6,6H60.5
		c-3.309,0-6-2.691-6-6v-9.996c0-3.309,2.691-6,6-6h9.996c3.309,0,6,2.691,6,6V54.998z"/>
	</svg>
	`;
}
export function login_password(color) {
    return `<?xml version="1.0" encoding="utf-8"?>
	<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
	<rect display="none" fill="#9B7E90" width="100" height="100"/>
	<g display="none">
		<g display="inline">
			<path fill=${color} d="M47,50.488h6c5.522,0,10-4.478,10-10v-6c0-5.522-4.478-10-10-10h-6c-5.522,0-10,4.478-10,10v6
				C37,46.011,41.477,50.488,47,50.488z M43,34.488c0-2.206,1.794-4,4-4h6c2.206,0,4,1.794,4,4v6c0,2.206-1.794,4-4,4h-6
				c-2.206,0-4-1.794-4-4V34.488z"/>
			<path fill=${color} d="M74.985,70.471c-0.01-8.829-7.168-15.982-15.999-15.982H41.014c-8.831,0-15.989,7.153-15.999,15.982
				l-0.029,0.018v1.023c0,2.21,1.791,4,4,4h11c1.104,0,2-0.896,2-2v-2c0-1.104-0.896-2-2-2h-8.923
				c0.493-5.057,4.767-9.023,9.951-9.023h17.972c5.185,0,9.458,3.967,9.95,9.023h-15.95c-1.105,0-2,0.896-2,2v2c0,1.104,0.895,2,2,2
				h18.028c2.209,0,4-1.79,4-4v-1.023L74.985,70.471z"/>
		</g>
	</g>
	<path fill=${color} d="M70.496,33.002H60.5c-6.627,0-12,5.373-12,12V47H19.504c-1.104,0-2,0.895-2,2v2c0,1.104,0.896,2,2,2h7v4
		c0,1.104,0.896,2,2,2h6c1.104,0,2-0.896,2-2v-4H48.5v1.998c0,6.627,5.373,12,12,12h9.996c6.627,0,12-5.373,12-12v-9.996
		C82.496,38.375,77.123,33.002,70.496,33.002z M76.496,54.998c0,3.309-2.691,6-6,6H60.5c-3.309,0-6-2.691-6-6v-9.996
		c0-3.309,2.691-6,6-6h9.996c3.309,0,6,2.691,6,6V54.998z"/>
	</svg>`;
}
