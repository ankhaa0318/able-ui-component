'use strict';

import { Dimensions, Platform } from 'react-native';
import { height8 } from '../../assets/dimensions/height';
const width69 = (Dimensions.get('window').width * 69) / 100;
const width72 = (Dimensions.get('window').width * 72) / 100;
const React = require('react-native');
let { StyleSheet } = React;
module.exports = StyleSheet.create({
    rootContainerStyle: {
        flex: 1,
        backgroundColor: '#3b4252',
        justifyContent: 'center',
    },
    textInputContainerStyle: {
        width: width72,
        alignSelf: 'center',
        color: 'white',
        fontFamily: 'Roboto-Bold',
        paddingBottom: 5,
        marginBottom: 10,
    },
    textInputWithIconContainer: {
        width: width69,
        alignSelf: 'center',
    },
    textInputStyle: {
        fontSize: 16,
        paddingBottom: Platform.OS === 'android' ? 10 : 8,
    },
    buttonStyle: {
        marginTop: 38,
        height: height8,
        width: width69,
        backgroundColor: '#d9584d',
    },
    labelContainer: {
        alignSelf: 'center',
        flexDirection: 'row',
        marginBottom: 35,
    },
    labelFirst: {
        color: 'white',
        fontSize: 20,
        fontFamily: 'Roboto-Bold',
        marginRight: 5,
    },
    labelSecond: {
        color: 'red',
        fontSize: 20,
        fontFamily: 'Roboto-Bold',
    },
    loaderStyle: {
        width: 50,
        height: 50,
        backgroundColor: 'red',
    },
    image: {
        width: 65,
        height: 72,
        alignSelf: 'center',
    },
    Atitle: {
        width: '100%',
        fontFamily: 'Roboto-Medium',
        fontSize: 18,
        textAlign: 'center',
        color: 'black',
        marginVertical: '5%',
    },
    Abody: {
        width: '80%',
        color: '#616161',
        fontSize: 14,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: '10%',
    },
    AlertModal: {
        padding: 20,
        borderRadius: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        paddingVertical: 40,
    },
    buttonLabel: {
        fontFamily: 'Roboto-Bold',
        marginBottom: 4,
    },
});
